from serverless_bottle_error_handling import __version__


def test_version():
    assert __version__ == '0.1.0'
