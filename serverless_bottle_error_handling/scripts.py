import subprocess
import argparse
from itertools import chain
from typing import List, Optional


def test():
    cmd = ["pytest", "-vvvvv"]

    exit(subprocess.run(cmd).returncode)


def test_only():

    parser = argparse.ArgumentParser(description="Run only the tests by mark")
    parser.add_argument("--filters", "-m", type=str, nargs="+")
    args = parser.parse_args()
    filters = list(chain(*[["-m", str(arg)] for arg in args.filters]))
    cmd: List[str] = ["pytest", "-vvvvv"]
    cmd.extend(filters)

    exit(subprocess.run(cmd).returncode)


def js_install():
    parser = argparse.ArgumentParser(description="Run yarn add")
    parser.add_argument("--dev", "-D", action="store_true")
    parser.add_argument("packages", nargs="+")
    cmd: List[str] = ["yarn", "add"]
    args = parser.parse_args()
    if args.dev:
        cmd.append("-D")
    cmd.extend(args.packages)
    exit(subprocess.run(cmd).returncode)


def js_remove():
    parser = argparse.ArgumentParser(description="Run yarn remove")
    parser.add_argument("--dev", "-D", action="store_true")
    parser.add_argument("packages", nargs="+")
    cmd: List[str] = ["yarn", "remove"]
    args = parser.parse_args()
    if args.dev:
        cmd.append("-D")
    cmd.extend(args.packages)
    exit(subprocess.run(cmd).returncode)


def all():
    print("test runs -- pytest with provided poetry verbosity level")
    print(
        "test-only -m filter [filter] ... [filter] -- runs pytest with provided poetry verbosity level, running only the tests matching @mark.<filter>"
    )
    print(
        "js-install [-D] [package] ... [package] -- runs yarn add with the package(s) listed, use -D to install in dev"
    )
    print(
        "js-remove [-D] [package] ... [package] -- runs yarn remove with the package(s) listed, use -D to install in dev"
    )
    print(
        "serverless -serveless_args [serveless args] -- runs serverless functions. All passed args are passed verbatim to the serverless command"
    )
    exit(0)


def serverless():
    parser = argparse.ArgumentParser(description="Run serverless commands")
    parser.add_argument("serverless_args", nargs="*")
    cmd: List[str] = ["serverless"]
    args = parser.parse_args()
    cmd.extend(args.serverless_args)
    exit(subprocess.run(cmd).returncode)
