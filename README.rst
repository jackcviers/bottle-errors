=================================
 serverless-bottle-erro-handling
=================================

Prereqs
-------

1.  NVM
2.  Poetry
3.  Type nvm use in this directory at the cli to ensure you are using the correct node version.
4.  Run `poetry update && poetry run js-install global yarn && poetry run js-install update`  on the first run/clone.
5.  Run `poetry run all` to see a list of available custom commands.

Adding build commands
---------------------

Everything is managed with `Poetry`. To add a custom command, add a `subprocess` def to the `serverless_bottle_error_handling.scripts` module (see the already added processes for examples and the built-in module argparse for examples of argument parsing), add a println to the all subprocess explaining its usage, and add a reference to the module with a name in `pyproject.toml` under `[tool.poetry.scripts]`. Running everything this way will ensure that things can be tested if necessary with pytest, keeps all the lockfiles up to date, and ensures everything is run within the correct environment containers.

