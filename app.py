from bottle import Bottle
from serverless_bottle_error_handling.error_handling_app import ErrorHandlingApp

# app has to be named app
app = ErrorHandlingApp(catchall=False, autojson=True)

from serverless_bottle_error_handling.routes import *
