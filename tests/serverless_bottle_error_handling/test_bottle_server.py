from __future__ import annotations
from _thread import start_new_thread
from typing import Optional
from app import app
import time


class TestBottleServer(object):
    """
    Starts a local instance of bottle container to run the tests against.
    """

    is_running = False
    _server: Optional[TestBottleServer] = None

    def __init__(
        self,
        app=app,
        host="localhost",
        port=3534,
        debug=False,
        reloader=False,
        server="tornado",
    ):
        self.app = app
        self.host = host
        self.port = port
        self.debug = debug
        self.reloader = reloader
        self.server = server

    @staticmethod
    def ensured_bottle_started():
        if TestBottleServer.is_running is False:
            server = TestBottleServer()
            start_new_thread(server.__start_bottle__, (), {})
            # Sleep is required for forked thread to initialise the app
            TestBottleServer.is_running = True
            time.sleep(1)
            TestBottleServer._server = server
        return TestBottleServer._server

    def __start_bottle__(self):
        self.app.run(host=self.host, port=self.port)

    @staticmethod
    def restart():
        TestBottleServer.is_running = False
        return TestBottleServer.ensured_bottle_started()
