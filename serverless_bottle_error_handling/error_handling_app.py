import sys
from bottle import Bottle, response, unicode, html_escape, tob, DEBUG, _e, format_exc
import json
import typing
from logging import Logger, getLogger


class ErrorHandlingApp(Bottle):
    def logger(self) -> Logger:
        return getLogger("ErrorHandlingApp")

    def __init__(self, catchall=True, autojson=True):
        super().__init__(catchall=catchall, autojson=autojson)

    def wsgi(self, environ, start_response):
        """The bottle WSGI-interface."""
        try:
            out = self._cast(self._handle(environ))
            # rfc2616 section 4.3
            if (
                response._status_code in (100, 101, 204, 304)
                or environ["REQUEST_METHOD"] == "HEAD"
            ):
                if hasattr(out, "close"):
                    out.close()
                out = []
            start_response(response._status_line, response.headerlist)
            return out
        except (KeyboardInterrupt, SystemExit, MemoryError):
            raise
        except Exception as appException:
            start_response(
                "500 Internal Server Error",
                [
                    ("Content-Type", "application/json"),
                ],
            )
            return [
                tob(
                    json.dumps(
                        {
                            "error": "Internal Server Error",
                            "details": appException.args,
                        }
                    )
                )
            ]
