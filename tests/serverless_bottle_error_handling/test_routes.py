import json
from tests.serverless_bottle_error_handling.test_bottle_server import TestBottleServer
from pytest import fixture, mark
import requests


@fixture(scope="module")
def server() -> TestBottleServer:
    return TestBottleServer.restart()


@mark.focus
def test_doesnt_throw_route(server: TestBottleServer):
    response = requests.get(f"http://{server.host}:{server.port}/doesntthrow")
    assert response.status_code == 200
    assert json.loads(response.content) == {"hello": "world"}


@mark.focus
def test_throws_route(server: TestBottleServer):
    response = requests.get(f"http://{server.host}:{server.port}/throws")
    assert response.status_code == 500
    assert json.loads(response.content) == json.loads(
        json.dumps(
            {
                "error": "Internal Server Error",
                "details": ("This resource does not exist",),
            }
        )
    )
