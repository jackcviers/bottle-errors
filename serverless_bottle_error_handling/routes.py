import json
from app import app
from bottle import response


@app.route("/throws")
def throws() -> str:
    raise Exception("This resource does not exist")


@app.route("/doesntthrow")
def doesntthrow() -> str:
    response.content_type = "application/json"
    return json.dumps({"hello": "world"})
